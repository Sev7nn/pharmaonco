import React, { useState, useEffect } from 'react';
import './App.css';
import Selector from './component/selector.js';
import ButtonAppBar from './component/navbar.js';
import SpaIcon from '@material-ui/icons/Spa';
import BubbleChartIcon from '@material-ui/icons/BubbleChart';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import List from '@material-ui/core/List';
import Box from '@material-ui/core/Box';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Selection_list from './component/selection_list';
import ContainerChimio from './component/container_chimio.js';
import ContainerClinicalTrials from './component/container_ct.js';


function App() {
  
  const [sorted_plant_list, setSortedPlantList] = useState(
    []
  );
  const [selection, setSelection] = useState(
    []
  );
  const [sorted_drug_list, setSortedDrugList] = useState(
    []
  );
  const [comparison_result, setComparisonResult] = useState(
    []
  );
  const [clinicaltrials, setClinicaltrials] = useState(
    []
  );


  
  
  function handleCompareChange(listOfitems) {

      axios.post('http://127.0.0.1:5000/MaxAllResults',
      {
        method:'POST',
        data: {listOfitems}
      })
        .then(function (response) {
         
          setComparisonResult([])
          setComparisonResult("Veuillez selectionner au moins une plante et une chimiothérapie.")
          //console.log(response.data.info)
          if (response.data.info=="no interaction"){
            if (response.data.data.length>1){
              //console.log('echnatillon sup a 1')
              setComparisonResult([])
              setComparisonResult("Aucunne donnée disponible pour votre sélection.")
              }
          }
          else if (response.data.info="interation"){
            //console.log(response.data);
            let resultComparison=response.data
            //console.log("-----DATA RESPONSE-----")
            //console.log(resultComparison)
            //console.log(resultComparison.responseMax)
            //console.log("-----------------------")

            setComparisonResult([])
            setComparisonResult(resultComparison)
          }
          
          else{
            setComparisonResult([])
          }

        })
        .catch(function (error) {
          console.log(error);
    }, []);
  }

  function handleCompareChangeTrial(listOfitems) {

    axios.post('http://127.0.0.1:5000/getClinicalTrials',
    {
      method:'POST',
      data: {listOfitems}
    })
      .then(function (response) {
        if (response.data.data){
          //console.log(response.data.data)
          setClinicaltrials(response.data.data)
        }
       
      })
      .catch(function (error) {
        console.log(error);
  }, []);
}


  function handleSelectionChange(new_value){
    if(new_value[0].type === 'onco'){
      setSortedDrugList(sorted_drug_list.filter(e => e.value !== new_value[0].value));
    }else if(new_value[0].type === 'plant'){
      setSortedPlantList(sorted_plant_list.filter(e => e.value !== new_value[0].value));
    }

    if (selection){
      if (selection.filter(e => e.value === new_value).length > 0){
        console.log("doublon")
      }
    }
   
      //console.log(new_value[new_value.length-1])
      let new_selection 
      if (new_value && selection){
        new_selection = selection.concat(new_value[new_value.length-1])
      }
      
      setSelection(new_selection)
      handleCompareChange(new_selection)
      handleCompareChangeTrial(new_selection)
      //setComparaison(new_selection)
      
   
  }

function handleDeletedItem(item){
  if(item.type === 'onco'){
    let drugList = sorted_drug_list;
    drugList.push(item);
    drugList.sort((a,b) => {
      return (a.value > b.value) ? 1 : -1;
    });
    setSortedDrugList(drugList);

  }else if (item.type === 'plant'){
    let plantList = sorted_plant_list;
    plantList.push(item);
    plantList.sort((a,b) => {
      return (a.value > b.value) ? 1 : -1;
    })

    setSortedPlantList(plantList)
  }
}

  function handleDeletion(value){
    if (selection){
      //console.log(value)
        for (var i= 0; i < selection.length; i++) { 
          if (selection[i].value === value) {
            let new_selection = selection.slice(0);
            new_selection.splice(i,1)
            console.log("element à supprimer : "+ selection[i].value)
            selection.splice(i,1)
            setSelection(new_selection)
            handleCompareChange(new_selection)
          }
          
        }
        //console.log("element suprr"+ new_selection)
    } 
    if (sorted_drug_list){ 
      for (var i=0; i<sorted_drug_list.length;i++){
        if (sorted_drug_list[i].value === value){
          //console.log('correspondance avec')
          //console.log(sorted_drug_list[i].value)
          //let new_option_list=sorted_drug_list.slice(0)
          //new_option_list.splice(i,1)
        }
      }
      
    }
    
 
      
    //console.log(value)
  }

  
  useEffect(() => {
    axios.get('http://127.0.0.1:5000/get_plant_list')
      .then(function (response) {
        //console.log(response);
        const new_sorted_plant_list = response['data'][0].map(data => (
          {
            value: data.label,
            label: data.label,
            type: data.type
          })
        )
        setSortedPlantList(new_sorted_plant_list);
        //console.log(new_sorted_plant_list) // set state 
      })
  }, []);

 
  useEffect(() => {
    axios.get('http://127.0.0.1:5000/get_drug_list')
      .then(function (response) {
        //console.log(response);
        const new_sorted_drug_list = response['data'][0].map(data => (
          {
            value: data.label,
            label: data.label,
            type: data.type
          })
        )
        setSortedDrugList(new_sorted_drug_list);
        //console.log(new_sorted_drug_list) // set state 
      })
  }, []);

  return (
    <section>
      <div><ButtonAppBar> PharmaOnco</ButtonAppBar></div>

      <div className="App">
      
         <Grid container direction="row" align="center" spacing={0}>
            <Grid item xs={12} sm={12} md={4} lg={4} align="center" id="wrapper_select">
                <Grid item xs={12} sm={12} md={11} lg={11} align="center" id="selectors" className="menuItem">
                  <Grid item xs={12} sm={12} md={12} lg={12} className="selectorItem">
                    <Grid item>
                      
                      <List >
                      <Container id="plant_selector" className="wrapperSelectorItem">
                        <ListItem>
                          <ListItemIcon>
                            <SpaIcon style={{color:'#5cd65c'}} />
                          </ListItemIcon>
                          <ListItemText primary="Phytothérapie" />
                        </ListItem>
                        <Grid item > 
                          <Selector options={sorted_plant_list} onSelectionChange={handleSelectionChange} label="Phytothérapie" placeHolder="Rechercher une Plante" >         
                          </Selector>
                        </Grid>
                      </Container>

                      <Box m={2} /> 

                      <Container id="onco_selector">
                        <ListItem>
                          <ListItemIcon >
                            <BubbleChartIcon style={{color:'#9b21ff'}}/>
                          </ListItemIcon>
                          <ListItemText primary="Chimiothérapie" />
                        </ListItem>
                        <Grid item > 
                          <Selector options={sorted_drug_list} onSelectionChange={handleSelectionChange} label="Chimiothérapie" placeHolder="Rechercher un Anticancéreux">         
                          </Selector>
                        </Grid>
                      </Container>
                      </List>
                    </Grid>
                  </Grid>
                </Grid> 

                <Grid item xs={10} sm={10} md={11} lg={11} align="center" className="menuItem">
                  <Selection_list handleDeletedItem={handleDeletedItem} item_list={selection} deleteItem={handleDeletion}></Selection_list>
                </Grid>

            </Grid>
            <Grid item xs={12} sm={12} md={8} lg={8} align="center" id="wrapper_result">
                <Grid item xs={11} sm={11} md={11} lg={9} align="center">
                    <ContainerClinicalTrials id="viewFirstResult" result={clinicaltrials}/>
                </Grid>
                <Grid item xs={11} sm={11} md={11} lg={9} align="center">
                    <ContainerChimio id="viewFirstResult" result={comparison_result}/>
                </Grid>
            </Grid>


         </Grid>



       
      </div>
    </section>
  );
}

export default App;


