import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import PropTypes, { func } from 'prop-types';
import Grid from '@material-ui/core/Grid';
import RiskIntensityItem from './RiskIntensityItem.js'
import RenderAllMaxIntensity from './render_results.js';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';

function getColorfromIntensity(intensity){
    let color= ""
    switch(intensity){

        case 100:
            //console.log("couleur vert")
            color= "rgba(207, 237, 185,0.6)"
            break;
        case 200:
            //console.log("couleur orange")
            color= "rgba(255, 194, 89,0.6)"
            break;
        case 300:
            //console.log("couleur rouge")
            color= "rgba(186, 37, 37,0.6)"
            break;
    
        return color
    }
}


export default function ContainerChimio(props) {

    
    if (props.result['allMax']){

        return (
        <div>
            <div>            
            { props.result['allMax'].map((items) => {
                //console.log(items[1])
                    return ( 
                        <Card className='wrapperResult' key={items[0]} style={{border:'solid 1px #d9d9d9',borderRadius:'10px',margin:'0px'}}>
                            <CardHeader 
                            title={items[0]} 
                            className='HeaderCard'>
                            </CardHeader>
                                <RenderAllMaxIntensity 
                                plants={items[1]}
                                > 
                                </RenderAllMaxIntensity> 
                        </Card>
                            )
                     }
            )}
         </div>
        </div>        
    ) 
    }

   
    else{
        //console.log(props.result)
        if (props.result.length>0){
            return (
            <Card className='wrapperResultNoSelection' style={{border:'solid 1px #d9d9d9',borderRadius:'10px',margin:'10px'}}>
                            <CardHeader 
                            title={props.result} 
                            className='HeaderCard'
                            style={{fontSize:'15px!important'}}>
                            </CardHeader>

                        </Card>
        )}
        else{
            return (
                <Card className='wrapperResultNoSelection' style={{border:'solid 1px #d9d9d9',borderRadius:'10px',margin:'10px'}}>
                                <CardHeader 
                                title="Veuillez selectionner au moins une plante et une chimiothérapie."
                                className='HeaderCard'
                                style={{fontSize:'15px!important'}}>
                                </CardHeader>
                            </Card>
            )
        }
        
    }
}