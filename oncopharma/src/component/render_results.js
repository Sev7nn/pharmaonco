import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import PropTypes, { func } from 'prop-types';
import Grid from '@material-ui/core/Grid';
import RiskIntensityItem from './RiskIntensityItem.js'


export default function RenderResults(props) {
    if (props){ 
        return (
        <div>
            <div>
            { props.plants.map((items,index) => {
                //console.log(items)
                    switch(items[2]){
                        case 100:
                            //console.log("couleur vert")
                            items[2]= "rgba(207, 237, 185,0.3)"
                            break;
                        case 200:
                            //console.log("couleur orange")
                            items[2]= "rgba(255, 194, 89,0.3)"
                            break;
                        case 300:
                            //console.log("couleur rouge")
                            items[2]= "rgba(186, 37, 37,0.3)"
                            break;
                    }
                    //console.log(items[2])
                    //console.log(items[0])
                    return ( 
                        
                    <RiskIntensityItem 
                        background={items[2]}
                        key={index}
                        systems={items}
                    > 
                    </RiskIntensityItem> 
                            )
                 }
             )}
         </div>
        </div>       
    ) 
    }
    else{
        return (
        <div></div>
        )
    }
}