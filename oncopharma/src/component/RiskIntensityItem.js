import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import PropTypes, { func } from 'prop-types';
import Grid from '@material-ui/core/Grid';


export default function RiskIntensityItem(props) {
   if (props.systems){
       return (
        <div>
            <div className='plantContainer' style={{borderColor:props.systems[2],backgroundColor:props.background}}>
                <p>{props.systems[0]}</p>
                {props.systems[1].map((items,index) => {
                    return(
                    <p key={index}>
                        {items}
                    </p>)
                    }
                )
            }
            </div>
        </div>
    )
    ;}
    else{
        return (
            <div>
                Item vide
            </div>
        )
    }
    
}