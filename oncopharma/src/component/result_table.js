import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
  },
  table: {
   
  },
}));

function createData(plant, interaction, drug) {
  return {plant, interaction, drug};
}

const rows = [
  createData('Plante1', "'interaction 1", "drug2"),
  createData('Plante2', "'interaction 1", "drug2"),
  createData('Plante3', "'interaction 1", "drug4"),
  createData('Plante4', "'interaction 1", "drug5"),
  createData('Plante5', "'interaction 1", "drug7"),
];

export default function ResultTable(props) {
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Plante</TableCell>
            <TableCell align="center">Intéraction</TableCell>
            <TableCell align="right">Molécule</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {props.plantsSelected.map(row => (
            <TableRow key={row.label}>
              <TableCell component="th" scope="row">
                {row.label}
              </TableCell>
              <TableCell align="center">interaction</TableCell>
              <TableCell align="right">drug</TableCell>
            </TableRow>
          ))}
          {rows.map(row => (
            <TableRow key={row.plant}>
              <TableCell component="th" scope="row">
                {row.plant}
              </TableCell>
              <TableCell align="center">{row.interaction}</TableCell>
              <TableCell align="right">{row.drug}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
}