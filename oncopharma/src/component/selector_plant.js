import '../App.css';
import PropTypes from 'prop-types';
import Select from 'react-select';
import { emphasize, makeStyles, useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import NoSsr from '@material-ui/core/NoSsr';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import axios from 'axios';
import clsx from 'clsx';
import React, { useState, useEffect } from 'react';
import Chip from '@material-ui/core/Chip';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import CancelIcon from '@material-ui/icons/Cancel';
import SpaIcon from '@material-ui/icons/Spa';
import BubbleChartIcon from '@material-ui/icons/BubbleChart';
import Grid from '@material-ui/core/Grid';


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    minWidth: 200,
    maxWidth: 752,
  },
  input: {
    display: 'flex',
    padding: 0,
    height: 'auto',
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center',
    overflow: 'hidden',
  },
  chip: {
    margin: theme.spacing(0.5, 0.25),
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
      0.08,
    ),
  },
  noOptionsMessage: {
    padding: theme.spacing(1, 2),
  },
  singleValue: {
    fontSize: 16,
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    bottom: 6,
    fontSize: 16,
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing(1),
    left: 0,
    right: 0,
  },
  divider: {
    height: theme.spacing(2),
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  }
}));



function NoOptionsMessage(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.noOptionsMessage}
      //{...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

NoOptionsMessage.propTypes = {
  /**
   * The children to be rendered.
   */
  children: PropTypes.node,
  /**
   * Props to be passed on to the wrapper.
   */
  //innerProps: PropTypes.object.isRequired,
  selectProps: PropTypes.object.isRequired,
};

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}

inputComponent.propTypes = {
  inputRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({
      current: PropTypes.any.isRequired,
    }),
  ]),
};

function Control(props) {
  const {
    children,
    innerProps,
    innerRef,
    selectProps: { classes, TextFieldProps },
  } = props;

  return (
    <TextField
      fullWidth
      InputProps={{
        inputComponent,
        inputProps: {
          className: classes.input,
          ref: innerRef,
          children,
          ...innerProps,
        },
      }}
      {...TextFieldProps}
    />
  );
}

Control.propTypes = {
  /**
   * Children to render.
   */
  children: PropTypes.node,
  /**
   * The mouse down event and the innerRef to pass down to the controller element.
   */
  innerProps: PropTypes.shape({
    onMouseDown: PropTypes.func.isRequired,
  }).isRequired,
  innerRef: PropTypes.oneOfType([
    PropTypes.oneOf([null]),
    PropTypes.func,
    PropTypes.shape({
      current: PropTypes.any.isRequired,
    }),
  ]).isRequired,
  selectProps: PropTypes.object.isRequired,
};

function Option(props) {
  return (
    <MenuItem
      ref={props.innerRef}
      selected={props.isFocused}
      component="div"
      style={{
        fontWeight: props.isSelected ? 500 : 400,
      }}
      {...props.innerProps}
    >
      {props.children}
    </MenuItem>
  );
}

Option.propTypes = {
  /**
   * The children to be rendered.
   */
  children: PropTypes.node,
  /**
   * props passed to the wrapping element for the group.
   */
  //innerProps: PropTypes.shape({
  //  id: PropTypes.string.isRequired,
  //  key: PropTypes.string.isRequired,
  //  onClick: PropTypes.func.isRequired,
  //  onMouseMove: PropTypes.func.isRequired,
  //  onMouseOver: PropTypes.func.isRequired,
  //  tabIndex: PropTypes.number.isRequired,
  //}).isRequired,
  /**
   * Inner ref to DOM Node
   */
  //innerRef: PropTypes.oneOfType([
  //  PropTypes.oneOf([null]),
  //  PropTypes.func,
  //  PropTypes.shape({
  //    current: PropTypes.any.isRequired,
  //  }),
  //]).isRequired,
  /**
   * Whether the option is focused.
   */
  isFocused: PropTypes.bool.isRequired,
  /**
   * Whether the option is selected.
   */
  isSelected: PropTypes.bool.isRequired,
};

function Placeholder(props) {
  const { selectProps, children } = props;
  return (
    <Typography color="textSecondary" className={selectProps.classes.placeholder} //{...innerProps}
    >
      {children}
    </Typography>
  );
}

Placeholder.propTypes = {
  /**
   * The children to be rendered.
   */
  children: PropTypes.node,
  /**
   * props passed to the wrapping element for the group.
   */
  innerProps: PropTypes.object,
  selectProps: PropTypes.object.isRequired,
};

function SingleValue(props) {
  return (
    <Typography className={props.selectProps.classes.singleValue} //{...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

SingleValue.propTypes = {
  /**
   * The children to be rendered.
   */
  children: PropTypes.node,
  /**
   * Props passed to the wrapping element for the group.
   */
  //innerProps: PropTypes.any.isRequired,
  selectProps: PropTypes.object.isRequired,
};

function ValueContainer(props) {
  return <div className={props.selectProps.classes.valueContainer}>{props.children}</div>;
}

ValueContainer.propTypes = {
  /**
   * The children to be rendered.
   */
  children: PropTypes.node,
  selectProps: PropTypes.object.isRequired,
};

function MultiValue(props) {
  return (
    <Chip
      tabIndex={-1}
      label={props.children}
      className={clsx(props.selectProps.classes.chip, {
        [props.selectProps.classes.chipFocused]: props.isFocused,
      })}
      onDelete={props.removeProps.onClick}
      deleteIcon={<CancelIcon {...props.removeProps} />}
    />
  );
}

MultiValue.propTypes = {
  children: PropTypes.node,
  isFocused: PropTypes.bool.isRequired,
  removeProps: PropTypes.shape({
    onClick: PropTypes.func.isRequired,
    onMouseDown: PropTypes.func.isRequired,
    onTouchEnd: PropTypes.func.isRequired,
  }).isRequired,
  selectProps: PropTypes.object.isRequired,
};

function Menu(props) {
  return (
    <Paper square className={props.selectProps.classes.paper} {...props.innerProps}>
      {props.children}
    </Paper>
  );
}

Menu.propTypes = {
  /**
   * The children to be rendered.
   */
  children: PropTypes.element.isRequired,
  /**
   * Props to be passed to the menu wrapper.
   */
  innerProps: PropTypes.object.isRequired,
  selectProps: PropTypes.object.isRequired,
};

const components = {
  Control,
  Menu,
  MultiValue,
  NoOptionsMessage,
  Option,
  Placeholder,
  SingleValue,
  ValueContainer,
};

export default function Selector_Plant(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [multi, setMulti] = React.useState(null);
  const [selection, setSelection] = React.useState([]);

  const handleChangeMulti = value => {
    setMulti(value);
    setSelection(value);
    props.getDataPlantList(value);
  };

  function deleteItem(value){
    const newSelection = selection.filter(item => item.value != value);
    setSelection(newSelection);
    props.getDataPlantList(newSelection);
  }

  // Equivate state : sorted_plant_list est dans le state et setSortedPlanList(newData) permet de set le state  
  const [sorted_plant_list, setSortedPlantList] = useState(
    []
  );
  const [sorted_drug_list, setSortedDrugList] = useState(
    []
  );
  useEffect(() => {
    axios.get('http://127.0.0.1:5000/get_plant_list')
      .then(function (response) {
        console.log(response);
        const new_sorted_plant_list = response['data'][0].map(data => (
          {
            value: data.label,
            label: data.label,
            type: data.type
          })
        )
        setSortedPlantList(new_sorted_plant_list); // set state 
      })
  }, []);

 
  useEffect(() => {
    axios.get('http://127.0.0.1:5000/get_drug_list')
      .then(function (response) {
        console.log(response);
        const new_sorted_drug_list = response['data'][0].map(data => (
          {
            value: data.label,
            label: data.label,
            type: data.type
          })
        )
        setSortedDrugList(new_sorted_drug_list); // set state 
      })
  }, []);


  const selectStyles = {
    input: base => ({
      ...base,
      color: theme.palette.text.primary,
      '& input': {
        font: 'inherit',
      },
    }),
  };
  


  if (selection) {
  
    var namesList = selection.map(function(state,index){
      var avatar;

      if ((state.type) === 'plant') {
        avatar = <Avatar style={{backgroundColor:'#f2f2f2'}}> 
                    <SpaIcon style={{color:'#5cd65c'}} />
                  </Avatar>;
      } else {
        avatar = <Avatar style={{backgroundColor:'#f2f2f2'}}> 
                   <BubbleChartIcon style={{color:'#9b21ff'}} />
                </Avatar>;
      }



    return <ListItem key={index}>
              <ListItemAvatar>
                {avatar}  
              </ListItemAvatar>
              <ListItemText 
              primary = {state.value}
              />
              <ListItemSecondaryAction onClick={e => deleteItem(state.value)}>
                <IconButton edge="end" aria-label="close" >
                <CancelIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
      }
    )
      }
  
  return (
    <div className={classes.root}>
      
    <Grid
    container
    direction="row"
    alignItems="center"
    justify="center"
  >
    <Grid container direction="row" align="center" spacing={10}>
    <Grid item xs={12} sm={6} md={6} id="selector_plant">
      <ListItemAvatar>
        <Avatar style={{backgroundColor:'#f2f2f2'}}> 
            <SpaIcon style={{color:'#5cd65c'}} />
        </Avatar>
      </ListItemAvatar>
      <NoSsr>
        <div className={classes.divider} />
        <Select
          classes={classes}
          styles={selectStyles}
          inputId="react-select-multiple"
          TextFieldProps={{
            label: 'Plante',
            InputLabelProps: {
              htmlFor: 'react-select-multiple',
              shrink: true,
            },
          }}
          placeholder="Rechercher"
          options={sorted_plant_list}
          components={components}
          value={multi}
          onChange={handleChangeMulti}
          isMulti
        />
      </NoSsr>

    </Grid>
    <Grid item xs={12} sm={6} md={6} id="selector_onco">
        <ListItemAvatar>
          <Avatar style={{backgroundColor:'#f2f2f2'}}> 
            <BubbleChartIcon style={{color:'#9b21ff'}} />
          </Avatar>  
          </ListItemAvatar>
          <NoSsr>
        <div className={classes.divider} />
        <Select
          classes={classes}
          styles={selectStyles}
          inputId="react-select-multiple"
          TextFieldProps={{
            label: 'Onco',
            InputLabelProps: {
              htmlFor: 'react-select-multiple',
              shrink: true,
            },
          }}
          placeholder="Rechercher"
          options={sorted_drug_list}
          components={components}
          value={multi}
          onChange={handleChangeMulti}
          isMulti
        />
      </NoSsr>

    </Grid>
    </Grid>
  </Grid>
    
    


      <div className={classes.root}>
      <Container>
          <div className={classes.demo}>
            <List>
            { namesList }
            </List>
          </div>
      </Container>
    </div>

    </div>
  );
}