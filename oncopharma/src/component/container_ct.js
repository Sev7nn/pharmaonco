import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import PropTypes, { func } from 'prop-types';
import Grid from '@material-ui/core/Grid';
import RiskIntensityItem from './RiskIntensityItem.js'
import RenderAllMaxIntensity from './render_results.js';
import Card from '@material-ui/core/Card';
import Avatar from '@material-ui/core/Avatar';
import CardHeader from '@material-ui/core/CardHeader';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
}));


function getColorfromIntensity(intensity){
    let color= ""
    switch(parseInt(intensity)){

        case 1:
            //console.log("couleur vert")
            color= "rgba(207, 237, 185,0.6)"
            return color;
        case 2:
            //console.log("couleur vert")
            color= "rgba(255, 194, 89,0.6)"
            return color;
        case 3:
            //console.log("couleur orange")
            color= "rgba(255, 194, 89,0.6)"
            return color;
        case 4:
            //console.log("couleur rouge")
            color= "rgba(186, 37, 37,0.6)"
            return color;
    

    }
}


export default function ContainerClinicalTrials(props) {

  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };
    //console.log(props)

    if (props.result){
        console.log(props.result)
        let i =0
        return (
            <div className={classes.root}>    
                 <div>
              
                    { props.result.map((items,index) => {
                        i++
                        let newId = 'panel'.concat(i,'bh-header')
                        let newControl = 'panel'.concat(i,'bh-content')
                        let newPanel = 'panel'.concat(i)
                            return ( 
                                    <ExpansionPanel key={index} expanded={expanded === newPanel} onChange={handleChange(newPanel)}>
                                        
                                        <ExpansionPanelSummary
                                        expandIcon={<ExpandMoreIcon/>}
                                        aria-controls={newControl}
                                        id={newId}
                                        >
                                        <Grid item xs={2}>
                                        <Avatar className={classes.avatar} style={{backgroundColor:getColorfromIntensity(items[0][2]),width:'70px',height:'70px'}}></Avatar>
                                        </Grid>
                                        <Grid item xs={10}>
                                        <Typography className={classes.heading} id="CT_title">{items[0][0]} - {items[0][1]}</Typography>
                                        <Typography className={classes.heading} variant="caption">{items[0][5]}</Typography>
                                        <Typography className={classes.secondaryHeading}>Risque d'intensité {items[0][2]}</Typography>
                                        </Grid>
                                        
                                       
                                        </ExpansionPanelSummary>
                                        
                                        <ExpansionPanelDetails>
                                            
                                            { items.map((item,index) => {
                                                console.log(i)
                                                    return ( 
                                                        <Grid container spacing={1} key={index}>
                                                            <Grid item xs={1}>

                                                                <Avatar className="CT_avatar_sub" style={{backgroundColor:getColorfromIntensity(item[2])}}>{item[2]}</Avatar>

                                                            </Grid>
                                                            <Grid item xs={11}>
                                                                <Typography variant="subtitle1" align="left" gutterBottom>
                                                                    {item[0]} - {item[1]} - {item[5]}
                                                                </Typography>
                                                                
                                                                <Typography variant="body2" align="left" gutterBottom>
                                                                    {item[4]}
                                                                </Typography>
                                                            </Grid>
                                                        </Grid>
                                                      )
                                                     }
                                                    )
                                                }
                                         
                                        </ExpansionPanelDetails>
                                    </ExpansionPanel>  

                            )
                     }
            )}
            </div>
        </div>        
    ) 
    }

   
    else{
        return (
                <div>Pas d'études cliniques recensées</div>
            )
        
        
    }
}