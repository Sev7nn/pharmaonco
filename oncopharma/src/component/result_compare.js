import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import PropTypes, { func } from 'prop-types';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2),
  },
}));

export default function PaperSheet(props) {
  const classes = useStyles();

  let firstResult = {props}.props.result
  let result = {props}
  //console.log(result.result)

  //console.log({props}.props)


  let backG= "rgba(196, 196, 196,0.1)"
  let bordR= "rgba(196, 196, 196,0.6)"
  let title = ""
  let info = ""
  let detail = ""
  let elt1, elt2 = ""





  if (firstResult['first']) {


    switch(firstResult['first'][1]){
      case 300:
        backG= "rgba(189, 11, 11,0.3)"
        bordR= "rgba(189, 11, 11,1)"
        title= "Association Interdite"
        info= ""
        break;
      case 200:
        backG= "rgba(230, 141, 39,0.4)"
        bordR= "rgba(230, 141, 39,0.8)"
        title= "Association déconseillée"
        info= ""
        break;
      case 100:
        backG= "rgba(137, 209, 13,0.4)"
        bordR= "rgba(137, 209, 13,0.8)"
        title= "Association possible"
        info= ""
        break;
      case 202:
        backG= "rgba(203, 157, 227,0.2)"
        bordR= "rgba(203, 157, 227,0.8)"
        title= "Association possible *"
        elt1 = String(firstResult['first'][0].split(' & ')[0])
        elt2 = String(firstResult['first'][0].split(' & ')[1])
        info= "* Aucunes données disponibles pour ces deux éléments :"
        detail = "".concat(elt1," & ",elt2)
        break;
      case 102:
        backG= "rgba(203, 157, 227,0.2)"
        bordR= "rgba(203, 157, 227,0.8)"
        title= "Association possible *"
        elt2 = String(firstResult['first'][0].split(' & ')[0])
        info= "* Aucunes données disponibles pour l'élement suivant:"
        detail = "".concat(elt2)
        break;
      case 201:
        backG= "rgba(203, 157, 227,0.2)"
        bordR= "rgba(203, 157, 227,0.8)"
        title= "Association possible *"
        elt1 = String(firstResult['first'][0].split(' & ')[0])
        info= "* Aucunes données disponibles pour l'élement suivant:"
        detail = "".concat(elt1)
        break;
    }

  //console.log(firstResult['first'][0].split(' & ')[0])
 // console.log(String(firstResult['first'][0].split(' & ')[0]))
    return (
      <Grid item xs={8} sm={8} md={11} lg={11} align="center" id="selectors" className="menuItem" style={{padding:10}}>
       <div >
      
        <Paper className={classes.root} style={{backgroundColor:backG,padding:5}}>
          <Typography variant="h5" component="h3" style={{fontSize:32,fontWeigt:900}}>
            {title}
          </Typography>
          <Typography component="p" style={{fontSize:22}}>
            {firstResult['first'][0]}
          </Typography>
          <Typography component="p">
            {firstResult['first'][2]}
          </Typography>
        </Paper>
        <div style={{height:8}}></div>
        {info && 
      <Paper className={classes.root} style={{backgroundColor:backG,padding:10}}>
      <Typography component="p">
        {info}
      </Typography>
      <Typography component="p">
        {detail}
      </Typography>
    </Paper>
      }
      </div>
    </Grid>
    );
  }
  else{
    return (
      <div>
      </div>
    );
  }
}