import '../App.css';
import PropTypes from 'prop-types';
import Select from 'react-select';
import { emphasize, makeStyles, useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import NoSsr from '@material-ui/core/NoSsr';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import axios from 'axios';
import clsx from 'clsx';
import React, { useState, useEffect } from 'react';
import Chip from '@material-ui/core/Chip';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import CancelIcon from '@material-ui/icons/Cancel';
import SpaIcon from '@material-ui/icons/Spa';
import BubbleChartIcon from '@material-ui/icons/BubbleChart';
import Grid from '@material-ui/core/Grid';
import AddCircleIcon from '@material-ui/icons/AddCircle';





export default function Selection_list(props) {

   
    if (props.item_list) {
        var namesList = props.item_list.map(function(state,index){
          var avatar;
          if (state){
            if ((state.type) === 'plant') {
                avatar = <Avatar style={{backgroundColor:'#f2f2f2'}}> 
                            <SpaIcon style={{color:'#5cd65c'}} />
                        </Avatar>;
            } 
            else {
                avatar = <Avatar style={{backgroundColor:'#f2f2f2'}}> 
                        <BubbleChartIcon style={{color:'#9b21ff'}} />
                        </Avatar>;
            }

    function deleteItem(item){
        props.deleteItem(item.value);
        props.handleDeletedItem(item);
    }
          
    return <ListItem key={index}>
                <ListItemAvatar>
                {avatar}  
                </ListItemAvatar>
                <ListItemText 
                primary = {state.value}
                />
                <ListItemSecondaryAction onClick={e => deleteItem(state) }>
                <IconButton edge="end" aria-label="close" style={{color:'rgba(59, 134, 255,0.8)',fontSize:'40px'}}>
                <CancelIcon />
                </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        }
    }
    )
    }
    if (props.item_list){
        if (props.item_list.length === 0) {
            //console.log("pas d'element a afficher")
            let askSelection = <ListItem >
                <Grid container align="center" spacing={0}>
                <Grid item xs={12}>
                    <ListItemAvatar>
                        <Avatar style={{backgroundColor:'white',fontSize:'40px'}}> 
                            <AddCircleIcon style={{color:'rgba(59, 134, 255,0.8)',fontSize:'40px'}} />
                        </Avatar>  
                    </ListItemAvatar>
                    <ListItemText
                    id="blankSelection"
                    style={{color:'rgba(59, 134, 255,0.8)',fontSize:2}}
                    primary = "Selectionnez un élément parmis ceux proposés"
                    />

                </Grid>
            </Grid>
            </ListItem>
            return askSelection
            
        }
    }
    return  <div>
                <Container id="selection">
                    <div >
                    <List>
                    { namesList }
                    </List>
                    </div>
                </Container>
            </div>
}