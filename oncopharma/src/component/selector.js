import React from 'react';

import { emphasize, makeStyles, useTheme } from '@material-ui/core/styles';
import NoSsr from '@material-ui/core/NoSsr';
import { useState, useEffect } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import Select from 'react-select';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    height: 70,
    minWidth: 200,
  },
  input: {
    display: 'flex',
    padding: 0,
    height: 'auto',
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center',
    overflow: 'hidden',
  },
  chip: {
    display:'none',
    margin: theme.spacing(0.5, 0.25),
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
      0.08,
    ),
  },
  noOptionsMessage: {
    padding: theme.spacing(1, 2),
  },
  singleValue: {
    fontSize: 16,
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    bottom: 6,
    fontSize: 16,
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing(1),
    left: 0,
    right: 0,
  },
  divider: {
    height: theme.spacing(2),
  },
}));


export default function Selector(props) {

  const classes = useStyles();
  const theme = useTheme();

  const handleChangeSelect = item => {
    props.onSelectionChange([item])
  };

 
  //console.log("--{props}--")
  //console.log({props})
  //console.log("--(props)--")
  //console.log(props.options)


  const selectStyles = {
    input: base => ({
      ...base,
      color: theme.palette.text.primary,
      '& input': {
        font: 'inherit',
      },
    }),
  };

 
  return (
    <div className={classes.root}>
      <NoSsr>
        <Select
         placeholder= {props.placeHolder}
         classes={classes}
         styles={selectStyles}
          className="basic-single"
          classNamePrefix="select"
          name="color"
          value={null}
          options={props.options}
          onChange={handleChangeSelect}
        />
      </NoSsr>
    </div>
  );
}