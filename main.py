from flask import Flask, render_template, request
from flask import jsonify
from flask_mysqldb import MySQL
from flask_restful import Resource, Api
from flask_cors import CORS, cross_origin
import pandas as pd
import json
import math

app = Flask(__name__)
cors = CORS(app, resources={r"/foo": {"origins": "*"}})

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'root'
app.config['MYSQL_DB'] = 'oncopharma'

mysql = MySQL(app)


def tranformToInt(coupleName,coupleIntensity):
    a=0
    b=0
    Unknow1=""
    Unknow2=""

    if (coupleIntensity[0]=="aucune"):
        a=1
    elif (coupleIntensity[0]=="faible" or coupleIntensity[0]=="G1"):
        a=2
    elif (coupleIntensity[0]=="moyenne" or coupleIntensity[0]=="G2"):
        a=3
    elif (coupleIntensity[0]=="forte" or coupleIntensity[0]=="G3" or coupleIntensity[0]=="G4"):
        a=4
    elif (coupleIntensity[0]=="--" or coupleIntensity[0]=='inconnue'):
        Unknow1=coupleName[0]
        #print("----Inconnue--Onco---")
        #print(Unknow1)
        #print("-----------------")
        a=2
    else:
        a=6
    

    if (coupleIntensity[1]=="aucune"):
        b=1
    elif ( coupleIntensity[1]=="faible" or coupleIntensity[1]=="G1"):
        b=2
    elif (coupleIntensity[1]=="moyenne" or coupleIntensity[1]=="G2"):
        b=3
    elif (coupleIntensity[1]=="forte" or coupleIntensity[1]=="G3" or coupleIntensity[1]=="G4"):
        b=4
    elif (coupleIntensity[1]=="--" or coupleIntensity[1]=='inconnue'):
        Unknow2=coupleName[1]
        #print("----Inconnue--Plant---")
        #print(Unknow2)
        #print("-----------------")
        b=2
    else :
        b=6

    return [[a,b],[Unknow1,Unknow2]]

def risk_matrix(list):     #      a = onco  b = plant

    a=list[0][0]
    b=list[0][1]

    ######### Risque medicament 4
    if (a==4) and ((b==4) or (b==3)):
        list.append(300)
        return list
    elif (a==4) and (b==2):
        list.append(200)
        return list
    elif (a==4) and (b==1):
        list.append(100)
        return list

    ######### Risque medicament 3
    elif (a==3) and ((b==4) or (b==3)):
        list.append(300)
        return list
    elif (a==3) and (b==2):
        list.append(200)
        return list
    elif (a==3) and (b==1):
        list.append(100)
        return list

    ######### Risque medicament 2
    elif (a==2) and (b==4):
        list.append(300)
        return list
    elif (a==2) and ((b==3) or (b==2)):
        list.append(200)
        return list
    elif (a==2) and (b==1):
        list.append(100)
        return list

    ######### Risque medicament 1
    elif (a==1) and ((b==4) or (b==3) or (b==2) or (b==1)):
        list.append(100)
        return list

    #Gestion des inconnues : inutile pour cette API
    #elif (a==0) or (b==0):
    #    loopForIntensity=[]
    #    if (a==0 and b==0):
    #        return 202   # OUI OUI
    #    if (a==0) and (b!=0):
    #        return 201   # OUI NON
    #    if (a!=0) and (b==0):
    #        return 102   #  NON OUI

def oneResponse(list):
    result=""
    if (len(list)>2):
        for i in range(len(list)-2):
            result=list[i]
            if list[i][1]>list[i+1][1]:
                result= list[i]
            else:
                result=list[i+1]
    elif (len(list)==2):
        if list[0]>list[1]:
            result=list[0]
        else:
            result=list[1]
    elif (len(list)==1):
        result=list[0]
    else :
        result=""
    return result

        











@app.route('/', methods=['GET'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def index():

    return render_template('index.html')
     

#   API pour recuperer plants
@app.route('/get_plant_list', methods=['GET', 'POST'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def plant_list():
    conn = mysql.connection
    cursor =conn.cursor()
    cursor.execute("SELECT list_name FROM data_plant")
    data = cursor.fetchall()
    list_plant = []
    sorted_plant_list = []
    for row in data:
        list_plant.append([row[0].capitalize(),"plant"])
        #list_plant.append(row[0].capitalize())
    df = pd.DataFrame(list_plant,columns=['value', 'type']).drop_duplicates()
    #df = pd.DataFrame({'plant':list_plant}).drop_duplicates()

    df = df.sort_values(by=['value'])
    for row in df.values.tolist():
        stock_values={}
        stock_values['label']=row[0]
        stock_values['type']=row[1]
        sorted_plant_list.append(stock_values)

    return jsonify([sorted_plant_list])


#   API pour recuperer drugs
@app.route('/get_drug_list', methods=['GET', 'POST'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def drug_list():
    conn = mysql.connection
    cursor =conn.cursor()
    cursor.execute("SELECT list_name FROM data_onco")
    data = cursor.fetchall()
    list_drug = []
    sorted_drug_list = []
    for row in data:
        list_drug.append([row[0].capitalize(),"onco"])
    df = pd.DataFrame(list_drug,columns=['value', 'type']).drop_duplicates()
    #df = pd.DataFrame({'plant':list_plant}).drop_duplicates()

    df = df.sort_values(by=['value'])
    for row in df.values.tolist():
        stock_values={}
        stock_values['label']=row[0]
        stock_values['type']=row[1]
        sorted_drug_list.append(stock_values)

    return jsonify([sorted_drug_list])
    
#
#
@app.route('/MaxAllResults', methods=['GET', 'POST'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def getAllAndMaxResults():
    data=request.json
    list_to_compare = []
    if data['data']['listOfitems']:
        if len(data['data']['listOfitems'])>0:
            list_to_compare = data['data']['listOfitems']
    plantList=[]
    oncoList=[]
    listElementsToCompare=[]
    resultIntensity=[]
    for item in list_to_compare:
        if item['type']=='onco':
            conn = mysql.connection
            cursor =conn.cursor()
            selecterbuilder="SELECT list_data_name,list_data_target,list_data_effect,list_data_intensity FROM data_onco WHERE list_name='{}'".format(item['value'])
            cursor.execute(selecterbuilder)
            data = cursor.fetchall()
            for elt in data:
                oncoList.append(elt)
        elif item['type']=='plant':
            conn = mysql.connection
            cursor =conn.cursor()
            selecterbuilder="SELECT list_data_name,list_data_target,list_data_effect,list_data_intensity FROM data_plant WHERE list_name='{}'".format(item['value'])
            cursor.execute(selecterbuilder)
            data = cursor.fetchall()
            for elt in data:
                plantList.append(elt)

        else :
            pass

    if (len(plantList) > 0) and (len(oncoList) > 0) :
        for plantData in plantList:
            if (plantData[2]!="substrat"):    #élimine les plantes substrats
                for oncoData in oncoList:
                    if oncoData[1].split()[-1] == '(PC)':   #selectionne les chimio PC
                        #print("----------Pharmacocinétique détécté")
                        if (oncoData[2]=='substrat') or (oncoData[2]=='inhibiteur, substrat') or (oncoData[2]=='inducteur, substrat'):
                            #print("+++++++++Pharmacocinétique = substrat")
                            #print("comparaison : {}  &  {}".format(oncoData[0],plantData[0]))
                            if (oncoData[1].replace(' (PC)','').replace(' (PD)','').replace(' (CAS)','').replace(' (EC)','').replace(' (EMA)','') == plantData[1]):
                                #print("sibling found in PC")
                                listElementsToCompare.append([[oncoData[0],plantData[0]],[oncoData[3],plantData[3]],oncoData[1]])
                    else :
                        if oncoData[1].split()[-1] != '(PC)':
                            if (oncoData[1].replace(' (PC)','').replace(' (PD)','').replace(' (CAS)','').replace(' (EC)','').replace(' (EMA)','') == plantData[1]):
                                #print("sibling found in no PC")
                                listElementsToCompare.append([[oncoData[0],plantData[0]],[oncoData[3],plantData[3]],oncoData[1]])


        #print("----les resultats-----")
        #print(listElementsToCompare)

        #for row in listElementsToCompare:
            #print(row)
            #resultIntensity.append()

        #print("----les resultats-----")
        #print(resultIntensity)
        #print("---------")
        for row in listElementsToCompare:
            #print("-----row-----")
            #print(row)
            #print("-----result by function-----")
            #print(tranformToInt(row[0],row[1]))
            #print(risk_matrix(tranformToInt(row[0],row[1])))
            result=risk_matrix(tranformToInt(row[0],row[1]))
            resultIntensity.append([result[2],result[0],result[1],row[0],row[2]])
        #print(resultIntensity)
        #print(len(resultIntensity))
        if (len(resultIntensity)>0):
            #print("condition ok")
            df = pd.DataFrame(resultIntensity,columns=['riskLevel','riskCouple','unknow','couple','target'])
            df = df.sort_values(by=['riskLevel'],ascending=False)
            resultAll=[]


            resultAllCoupleMax=[]
            setAllCouple = set(tuple(i) for i in df['couple'])
            #print(setAllCouple)
            for couple in setAllCouple:
                resultPerCouple=[]
                couplaMax=0
                for row in df.values:
                    if (row[3][0]==couple[0] and row[3][1]==couple[1]):
                        resultPerCouple.append(row)
                dfPerCouple = pd.DataFrame(resultPerCouple)
                couplaMax=dfPerCouple[0].max()
                #print(resultPerCouple)
                dfCoupleOnlyMax= dfPerCouple[dfPerCouple[0]==couplaMax]
                #print(dfCoupleOnlyMax)
                allTargets=[]
                for itemMaxPerCouple in dfCoupleOnlyMax.values:
                    #print(itemMaxPerCouple)
                    if (itemMaxPerCouple[2][0]) and (itemMaxPerCouple[2][1]):
                        allTargets.append("*"+itemMaxPerCouple[4]+"*")
                    elif (itemMaxPerCouple[2][0]) and not(itemMaxPerCouple[2][1]):
                        allTargets.append("*"+itemMaxPerCouple[4])
                    elif not(itemMaxPerCouple[2][0]) and (itemMaxPerCouple[2][1]):   
                        allTargets.append(itemMaxPerCouple[4]+"*")
                    elif not(itemMaxPerCouple[2][0]) and not(itemMaxPerCouple[2][1]):   
                        allTargets.append(itemMaxPerCouple[4])
                resultAllCoupleMax.append({'couple':couple,'target':allTargets,'intensity':couplaMax,'unkwows':itemMaxPerCouple[2]})
    
            notFusedData=pd.DataFrame(resultAllCoupleMax)

            listInteracOnco=[]
            for row in notFusedData.values:
                listInteracOnco.append(row[0][0])

            final_result=[]


            for item in set(listInteracOnco):
                merge_plant_interaction=[item.capitalize()]
                itemPerPlant=[]
                for row in notFusedData.values:
                    
                    #print(row)
                    #print('{}  &  {}'.format(item,row[0][0]))

                    if item== row[0][0]:
                        itemPerPlant.append([row[0][1],row[1],row[2]])
                sortItemPerPlant = pd.DataFrame(itemPerPlant,columns=['Plante','Systemes','Risque']).sort_values(["Risque","Plante"],ascending=[False,True])
                #print(sortItemPerPlant.values.tolist())
                merge_plant_interaction.append(sortItemPerPlant.values.tolist())
                final_result.append(merge_plant_interaction)
            #print('FINAL')
            #for item in final_result:
                #print(item)

            return jsonify({'info':'interaction',"allMax":final_result,"responseAll":resultAll})

    return jsonify({'info':'no interaction','data':list_to_compare})

###
##
##
@app.route('/getClinicalTrials', methods=['GET', 'POST'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def getClinicalTrials():
    data=request.json
    list_to_compare = data['data']['listOfitems']
    plantList=[]
    drugList=[]
    totalDataTrial=[]
    for item in list_to_compare:
        #print(item['value'])
        recapData=[]
        likeValue= item['value']
        if len(likeValue.split(" "))>1:
            likeValue=item['value'].split(" ")[0][:5]
        else :
            likeValue=item['value'][:5]

        if item['type']=='onco':
            drugList.append(item['value'])
            #print("medicament : {}".format(item['value']))

        elif item['type']=='plant':
            #print("plante : {}".format(item['value']))
            conn = mysql.connection
            cursor =conn.cursor()
            selecterbuilder="SELECT list_value,list_interaction,list_data,list_type FROM data_ec_cr WHERE list_value = '{}'".format(item['value'])
            
            cursor.execute(selecterbuilder)
            data = cursor.fetchall()

            for elt in data:
                plantList.append(elt)

    matchForCT=[]
    if len(plantList)>0:
        for plant in plantList:
            
            #print(json.loads(plant[1].replace('\'','\"'))['couple'])
            for drug in drugList:
                comparator_value=""
                if len(drug.replace(' (per os)','').replace(' (injectable)','').split(' '))>1:
                    coefComp= math.floor(len(drug.replace(' (per os)','').replace(' (injectable)','').split(' ')[0])*0.7)
                    if (drug.replace(' (per os)','').replace(' (injectable)','').split(' ')[0]).upper() == 'ACIDE':
                        #print("acide detecté : {}".format(drug))
                        comparator_value = drug.replace(' (per os)','').replace(' (injectable)','').split(' ')[0].upper()[:coefComp]
                        #print("Comparateur : {}".format(comparator_value))
                    else :
                        comparator_value = drug.replace(' (per os)','').replace(' (injectable)','').split(' ')[0].upper()[:coefComp]
                        #print("Comparateur : {}".format(comparator_value))
                else :
                    coefComp= math.floor(len(drug.replace(' (per os)','').replace(' (injectable)','').replace(" ",""))*0.7)
                    comparator_value = drug.replace(' (per os)','').replace(' (injectable)','').replace(" ","").upper()[:coefComp]
                    #print("Comparateur : {}".format(comparator_value))

                if any(comparator_value in s.upper() for s in (json.loads(plant[1].replace('\'','\"'))['couple'])):
                    #print("essaie trouvé")
                    #print(json.loads(plant[2].replace('{\'',"{\"").replace(', \'',", \"").replace('\':',"\":").replace(': \'',": \"").replace('\',',"\",").replace('\'}','\"}')))
                    #print(json.loads(plant[2].replace('{\'',"{\"").replace(', \'',",\"").replace('\':',"\":").replace(': \'',": \"").replace('\',',"\",")))
                    #print("")
                    #print(json.loads(plant[2].replace('{\'',"{\"").replace(', \'',", \"").replace('\':',"\":").replace(': \'',": \"").replace('\',',"\",").replace('\'}','\"}').replace('\\r\\n',' '), strict=False))
                    matchForCT.append([plant[0],
                                       drug,
                                       (tranformToInt([plant[0],drug],[json.loads(plant[2].replace('{\'',"{\"").replace(', \'',", \"").replace('\':',"\":").replace(': \'',": \"").replace('\',',"\",").replace('\'}','\"}').replace('\\r\\n',' '), strict=False)['intensity'],'aucune']))[0][0],
                                       json.loads(plant[2].replace('{\'',"{\"").replace(', \'',", \"").replace('\':',"\":").replace(': \'',": \"").replace('\',',"\",").replace('\'}','\"}').replace('\\r\\n',' '), strict=False)['effect'],
                                       json.loads(plant[2].replace('{\'',"{\"").replace(', \'',", \"").replace('\':',"\":").replace(': \'',": \"").replace('\',',"\",").replace('\'}','\"}').replace('\\r\\n',' '), strict=False)['info'],
                                       plant[3]
                                       ])
    

    if len(matchForCT)>0:
        #for item in matchForCT:
        #    print(item)
        #    print("")

        sortMatchingCT= pd.DataFrame(matchForCT,columns=['Plant','Drug','Intensity','Effect','Info','Type'])
        
        allMatchingPlants= set(sortMatchingCT["Plant"].tolist())
        classifiedMatch=[]
        #print(allMatchingPlants)
        for plant in allMatchingPlants:
            classer=[]
            for row in sortMatchingCT.sort_values(['Plant','Intensity','Type'],ascending=[True,False,False]).values.tolist():
                #print(row[0])
                #print(plant)
                #print("")
                if row[0].upper()==plant.upper():
                    classer.append(row)
            classifiedMatch.append(classer)
            totalDataTrial.append(classer)




                




    
    return jsonify({'data':totalDataTrial})


if __name__ == '__main__':
    app.run()